import 'dart:io';
import 'package:calendar_app_vol3/ui/home_page.dart';
import 'package:calendar_app_vol3/ui/theme.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try{
  if (Platform.isAndroid) {
    await Firebase.initializeApp();
  }}catch (e){
    print("upsi");
    await Firebase.initializeApp(options: const FirebaseOptions(
        apiKey:
        "AIzaSyALpgF8ouMrHRtfGZ_Y8ehO5BNxUGC9AUE",
        authDomain: "calendar-app-d933a-default-rtdb.firebaseio.com",
        databaseURL: "https://calendar-app-d933a-default-rtdb.firebaseio.com",
        appId: "1:841516595840:android:b6e985d5e4088176d70465",
        messagingSenderId: "841516595840-alu6vtf3ma51ialjqgkatbn93r0p91o6.apps.googleusercontent.com",
        projectId:  "calendar-app-d933a"));
  }
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {




  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: Themes.light,
      home: HomePage()
    );
  }
}
