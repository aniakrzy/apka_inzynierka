import 'dart:convert';
import 'dart:math';
import '';
import 'package:calendar_app_vol3/controllers/task_controller.dart';
import 'package:calendar_app_vol3/ui/add_test_form.dart';
import 'package:calendar_app_vol3/ui/task_tile.dart';
import 'package:calendar_app_vol3/ui/theme.dart';
import 'package:calendar_app_vol3/ui/widgets/button.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:firebase_core/firebase_core.dart';

import '../models/task.dart';

TaskController _taskController = Get.put(TaskController());
const Color primaryCrl = Color(0xFF4e5ae8);
const Color bluishCrl = Color(0xFF4e5ae8);
const Color yellowCrl = Color(0xFFFFB746);
const Color pinkCrl = Color(0xFFff4667);
const Color white = Colors.white;
const Color darkGrayCrl = Color(0xFF121212);
const Color darkHeaderyCrl = Color(0xFF424242);
DateTime _selectedDate = DateTime.now();

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! sranie w banie");
    int tasksCount = 0;
    return Scaffold(
      appBar: _appBar(context),
      body: Column(
        children: [
          _addTaskBar(context),
          _addDays(),
          const SizedBox(height: 10),
          _showTasks(context)
        ],
      ),
    );
  }
}

_showTasks(BuildContext context) {
  print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ive been called");
  _taskController.taskList.clear();
  _taskController.getTasks();
  return Expanded(child: Obx(() {
    return ListView.builder(
        itemCount: _taskController.taskList.length,
        itemBuilder: (_, index) {
          Task task =  _taskController.taskList[index];
          DateTime data = DateTime.parse(task.date);
          String dataFormatted = DateFormat.yMd().format(data);
          if (task.repeat=="Daily") {
            return AnimationConfiguration.staggeredList(
                position: index,
                child: SlideAnimation(
                  child: FadeInAnimation(child: Row(
                    children: [GestureDetector(onTap: () {
                      _showBottomSheet(context, task);
                    },
                      child: TaskTile(task)
                      ,)
                    ],
                  )),
                ));

          }if (dataFormatted.toString()==DateFormat.yMd().format(_selectedDate)){
            return AnimationConfiguration.staggeredList(
                position: index,
                child: SlideAnimation(
                  child: FadeInAnimation(child: Row(
                    children: [GestureDetector(onTap: () {
                      _showBottomSheet(context, task);
                    },
                      child: TaskTile(task)
                      ,)
                    ],
                  )),
                ));

          }else{
            return Container();
          }
        });
  }));
}

_bottomSheetButton({required String label, required Function()? onTap, required Color clr,
  bool isClose=false, required BuildContext context}){
  return GestureDetector(onTap: onTap,
    child: Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      height: 55,
      width: MediaQuery.of(context).size.width*0.9,
      decoration: BoxDecoration(
        border: Border.all(width: 2, color: isClose==true?Get.isDarkMode?Colors.grey[600]!:Colors.grey[300]!:clr,),
        borderRadius: BorderRadius.circular(20),
        color: isClose==true?Colors.transparent:clr,
      ),
      child: Center(
        child: Text(
          label,
          style: isClose==true?titleStyle:titleStyle.copyWith(color: Colors.white),
        ),
      ),
    ),);
}

_showBottomSheet(BuildContext context, Task task) {
  Get.bottomSheet(
      Container(
        padding: const EdgeInsets.only(top: 2),
        height: task.isCompleted == 1 ? MediaQuery
            .of(context)
            .size
            .height * 0.30 :
        MediaQuery
            .of(context)
            .size
            .height * 0.40,
          color: Colors.white,
          child: Column(
            children: [Container(height: 6, width: 120, decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey[300]
            )
            ),
          Spacer(),
          task.isCompleted==1?_bottomSheetButton(label: "Task not Complited!", onTap: (){
            Get.back();
          }, clr: primaryCrl, context: context):_bottomSheetButton(label: "Complete task!", onTap: (){
            _taskController.completeTask(task.id);
             Get.back();
          }, clr: primaryCrl, context: context,
          ),
              SizedBox(height: 10,),
              _bottomSheetButton(label: "Delete task", onTap: (){
                _taskController.deleteTask(task.id);
                Get.back();
              }, clr: Colors.red, context: context,
              ),
              SizedBox(height: 10,),
              _bottomSheetButton(label: "Close", onTap: (){
                Get.back();
              }, clr: Colors.red[300]!, isClose: true, context: context,
              ),
              SizedBox(height: 10,),
        ],
          ),

      )

  );
}

_addDays() {
  final Shader linearGradient = LinearGradient(
    colors: <Color>[Color(0xff8921aa), Color(0xff6fd6ff)],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 50.0, 10.0));
  return Container(
    margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
    child: DatePicker(
      DateTime.now(),
      height: 100,
      width: 80,
      initialSelectedDate: DateTime.now(),
      selectionColor: Colors.grey.shade50,
      selectedTextColor: Colors.purple,
      dateTextStyle: GoogleFonts.lato(
          textStyle: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w600, foreground: Paint()..shader = linearGradient)),
      dayTextStyle: GoogleFonts.lato(
          textStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, foreground: Paint()..shader = linearGradient)),
      monthTextStyle: GoogleFonts.lato(
          textStyle: TextStyle(
              fontSize: 14, fontWeight: FontWeight.w600, foreground: Paint()..shader = linearGradient)),
      onDateChange: (date) {
          _selectedDate = date;
      },
    ),
  );
}

_addTaskBar(BuildContext context) {
  return Container(
    decoration: const BoxDecoration(
        gradient: LinearGradient(
            colors: [Color(0xff6fd6ff),Color(0xff8c52ff)],
            begin: Alignment.bottomRight,
            end: Alignment.bottomLeft
        ),
  borderRadius: BorderRadius.only(
    bottomRight: Radius.circular(20),
    bottomLeft: Radius.circular(20),
    )),
    child: Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: Row(

        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  DateFormat.yMMMMd().format(DateTime.now()),
                  style: subHeadingStyle,
                ),
                Text("Today", style: headingStyle)
              ],
            ),
          ),
          MyButton(label: "+ Add Task", onTap: () async {
            await Navigator.push(
            context, MaterialPageRoute(builder: (context) => AddTaskForm()));
          })
        ],
      ),
    ),
  );
}

_appBar(BuildContext context) {
  return AppBar(
    elevation: 0,
    automaticallyImplyLeading: false,
    title: Row(
      children: [
        Image.asset("images/heartbeat_icon.png",
            color: Colors.white,
            width: 30,
            height: 30,
            fit: BoxFit.cover),
        SizedBox(
          width: 5,
          height: 10,
        ),
        Text("CareFully"),],


    ),
    actions: [
      IconButton(
        icon: Icon(Icons.settings),
        onPressed: (){},
      )
    ],
    flexibleSpace: Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xff6fd6ff),Color(0xff8c52ff)],
              begin: Alignment.bottomRight,
              end: Alignment.bottomLeft
          )

      ),
    ),

  );
}

