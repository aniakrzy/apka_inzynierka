import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';

const Color bluishCrl = Color(0xFF4e5ae8);
const Color yellowCrl = Color(0xFFFFB746);
const Color pinkCrl = Color(0xFFff4667);
const Color white = Colors.white;
const Color primaryCrl = bluishCrl;
const Color darkGrayCrl = Color(0xFF121212);
const Color darkHeaderyCrl = Color(0xFF424242);



class Themes{
  static final light= ThemeData(
    backgroundColor: Colors.white,
    primaryColor: primaryCrl,
    brightness: Brightness.light
  );


}


TextStyle get subHeadingStyle{
  return GoogleFonts.lato(
  textStyle: const TextStyle(
    color: Colors.white,
    fontSize: 24,
    fontWeight: FontWeight.bold,
      // color:Get.isDarkMode?Colors.white:Colors.black

  )
  );

}

TextStyle get headingStyle{
  return GoogleFonts.lato(
      textStyle: const TextStyle(
        color: Colors.white,
          fontSize: 30,
          fontWeight: FontWeight.bold,
          // color:Get.isDarkMode?Colors.white:Colors.black
      )
  );

}

TextStyle get titleStyle{
  return GoogleFonts.lato(
      textStyle: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w400,
        //color:Get.isDarkMode?Colors.white:Colors.black
      )
  );

}

TextStyle get subtitleStyle{
  return GoogleFonts.lato(
      textStyle: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: Colors.grey
      )
  );

}