import 'package:calendar_app_vol3/ui/theme.dart';
import 'package:calendar_app_vol3/ui/widgets/button.dart';
import 'package:calendar_app_vol3/ui/widgets/input_field.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:calendar_app_vol3/controllers/task_controller.dart';

TaskController _taskController = Get.put(TaskController());



class AddTaskForm extends StatefulWidget {
  const AddTaskForm({Key? key}) : super(key: key);

  @override
  State<AddTaskForm> createState() => _AddTaskFormState();
}

class _AddTaskFormState extends State<AddTaskForm> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();
  DateTime _selectedDate = DateTime.now();
  String _startTime = DateFormat("hh:mm a").format(DateTime.now());
  String _endTime = "12:00 AM";
  int _selectedRemind = 5;
  List<int> remindList = [5, 10, 15, 20];
  String _selectedRepeat = "None";
  List<String> repeatList = ["None", "Daily", "Weekly", "Monthly"];
  int _selectedColor = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _appBar(context),
      body: Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                "Add Task",
                style: headingStyle,
              ),
              MyInputField(
                  title: "Title",
                  hint: "Enter title",
                  controller: _titleController),
              MyInputField(
                  title: "Note",
                  hint: "Enter note",
                  controller: _noteController),
              MyInputField(
                title: "Date",
                hint: DateFormat.yMd().format(_selectedDate),
                widget: IconButton(
                  icon: const Icon(Icons.calendar_today_outlined),
                  onPressed: () {
                    _getDateFromUser();
                  },
                ),
              ),
              Row(children: [
                Expanded(
                  child: MyInputField(
                    title: "Start time",
                    hint: _startTime,
                    widget: IconButton(
                      icon: const Icon(Icons.access_time_rounded),
                      onPressed: () {
                        _getTimeFromUser(isStartTime: true);
                      },
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: MyInputField(
                    title: "End time",
                    hint: _endTime,
                    widget: IconButton(
                      icon: const Icon(Icons.access_time_rounded),
                      onPressed: () {
                        _getTimeFromUser(isStartTime: false);
                      },
                    ),
                  ),
                )
              ]),
              MyInputField(
                title: "Remind",
                hint: _selectedRemind.toString() + " minutes early",
                widget: DropdownButton(
                  icon: Icon(Icons.keyboard_arrow_down),
                  iconSize: 32,
                  elevation: 4,
                  style: subtitleStyle,
                  underline: Container(
                    height: 0,
                  ),
                  items: remindList.map<DropdownMenuItem<String>>((int value) {
                    return DropdownMenuItem<String>(
                      child: Text(value.toString()),
                      value: value.toString(),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _selectedRemind = int.parse(newValue!);
                    });
                  },
                ),
              ),
              MyInputField(
                title: "Repeat",
                hint: _selectedRepeat,
                widget: DropdownButton(
                  icon: Icon(Icons.keyboard_arrow_down),
                  iconSize: 32,
                  elevation: 4,
                  style: subtitleStyle,
                  underline: Container(
                    height: 0,
                  ),
                  items:
                      repeatList.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      child: Text(value),
                      value: value,
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      _selectedRepeat = newValue!;
                    });
                  },
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Opiekun",
                        style: titleStyle,
                      ),
                      Wrap(
                        children: List<Widget>.generate(
                            3,
                            (int index) => GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _selectedColor = index;
                                    });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: CircleAvatar(
                                      child: _selectedColor == index
                                          ? const Icon(
                                              Icons.done,
                                              color: Colors.white,
                                              size: 16,
                                            )
                                          : Container(),
                                      radius: 14,
                                      backgroundColor: index == 0
                                          ? primaryCrl
                                          : index == 1
                                              ? pinkCrl
                                              : yellowCrl,
                                    ),
                                  ),
                                )),
                      )
                    ],
                  ),
                  MyButton(label: "Create Task", onTap: () => _validateData())
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _appBar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      title: Row(
        children: [
          Image.asset("images/heartbeat_icon.png",
              color: Colors.white,
              width: 30,
              height: 30,
              fit: BoxFit.cover),
          SizedBox(
            width: 5,
            height: 10,
          ),
          Text("CareFully"),],


      ),
      actions: [
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: (){},
        )
      ],
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xff6fd6ff),Color(0xff8c52ff)],
                begin: Alignment.bottomRight,
                end: Alignment.bottomLeft
            )

        ),
      ),

    );
  }

  _validateData() async {
    if (_noteController.text.isNotEmpty && _titleController.text.isNotEmpty) {
      //add to database
      await _addToDB();
      print("VALIDATE DATA FUNC");
      _taskController.taskList.clear();
      Get.back();
    } else if (_noteController.text.isEmpty || _titleController.text.isEmpty) {
      Get.snackbar("Required", "All fields are required!",
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.white,
          colorText: Colors.black,
          icon: Icon(Icons.warning_amber_rounded));
    }
  }

  _addToDB() async {
    print("addToDB !!!!!!!!!!!!!!!!!!");
    int taskId = 0;
    //get id of the last added element
    DatabaseReference _testRef = FirebaseDatabase.instance.reference();
    //final ref = FirebaseDatabase.instance.refFromURL("https://calendar-app-d933a.firebaseio.com");
    final snapshot = await _testRef.child("counter").get();
    if (snapshot.exists) {
      taskId = snapshot.value as int;
    } else {
      print('No data available.');
    }
    taskId = (taskId + 1);
    //write data`

    DatabaseReference task =
    _testRef.child("tasks/task$taskId");
            task.set({
              "id": taskId,
      "title": "${_titleController.text}",
      "note": "${_noteController.text}",
      "isCompleted": 0,
      "date": "$_selectedDate",
      "startTime": "$_startTime",
      "endTime": "$_endTime",
      "remind": _selectedRemind,
      "color": _selectedColor,
      "repeat": "$_selectedRepeat",
    });
    _testRef.child("counter").set(taskId);
  }

  _getDateFromUser() async {
    DateTime? _pickDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2099));
    if (_pickDate != null) {
      setState(() {
        _selectedDate = _pickDate;
      });
    } else {}
  }

  _getTimeFromUser({required bool isStartTime}) async {
    var pickedTime = await _showTimePicker();
    String _fortmattedTime = pickedTime.format(context);

    if (pickedTime == null) {
    } else if (isStartTime == true) {
      setState(() {
        _startTime = _fortmattedTime;
      });
    } else if (isStartTime == false) {
      setState(() {
        _endTime = _fortmattedTime;
      });
    }
  }

  _showTimePicker() {
    return showTimePicker(
        context: context,
        initialEntryMode: TimePickerEntryMode.input,
        initialTime: TimeOfDay(
            hour: int.parse(_startTime.split(":")[0]),
            minute: int.parse(_startTime.split(":")[1].split(" ")[0])));
  }
}
