import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';

import '../models/task.dart';

class TaskController extends GetxController {
  @override
  void onReady() {
    super.onReady();
  }

  var taskList = <Task>[].obs;

  Future<void> getTasks() async {
    // List lista = <Task>[];
    print("Xd");
    taskList.clear();
    await FirebaseDatabase.instance
        .reference()
        .child("tasks")
        .onValue
        .listen((event) {
      for (final child in event.snapshot.children) {

        String newData = child.value.toString();
        print(newData);
        String temp = newData.split("color: ")[1];
        int color = int.parse(temp.split(",")[0]);
        temp = newData.split("date: ")[1];
        String date = temp.split(",")[0];
        temp = newData.split("endTime: ")[1];
        String endTime = temp.split(",")[0];
        temp = newData.split("id:")[1];
        int id = int.parse(temp.split(",")[0]);
        temp = newData.split("isCompleted: ")[1];
        int isCompleted = int.parse(temp.split(",")[0]);
        temp = newData.split("note:")[1];
        String note = temp.split(",")[0];
        temp = newData.split("remind: ")[1];
        String reminder = temp.replaceAll("}", "");
        int remind = int.parse(reminder);
        temp = newData.split("repeat: ")[1];
        String repeat = temp.split(",")[0];

        temp = newData.split("startTime: ")[1];
        String startTime = temp.split(",")[0];
        temp = newData.split("title: ")[1];
        String title = temp.split(",")[0];
        taskList.add(Task(id, title, note, isCompleted, date, startTime,
            endTime, color, remind, repeat));
      }
    });
  }

  Future<void> deleteTask(int id) async {
    await FirebaseDatabase.instance
        .reference()
        .child("tasks").child("task$id").remove();
   getTasks();
  }


  Future<void> completeTask(int id) async {
    await FirebaseDatabase.instance
        .reference()
        .child("tasks").child("task$id").child("isCompleted").set(1);
    getTasks();
  }
}
